object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 558
  ClientWidth = 1263
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 448
    Width = 1263
    Height = 110
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 0
    ExplicitWidth = 1684
    object SpinButton1: TSpinButton
      Left = 40
      Top = 37
      Width = 20
      Height = 25
      DownGlyph.Data = {
        0E010000424D0E01000000000000360000002800000009000000060000000100
        200000000000D800000000000000000000000000000000000000008080000080
        8000008080000080800000808000008080000080800000808000008080000080
        8000008080000080800000808000000000000080800000808000008080000080
        8000008080000080800000808000000000000000000000000000008080000080
        8000008080000080800000808000000000000000000000000000000000000000
        0000008080000080800000808000000000000000000000000000000000000000
        0000000000000000000000808000008080000080800000808000008080000080
        800000808000008080000080800000808000}
      TabOrder = 0
      UpGlyph.Data = {
        0E010000424D0E01000000000000360000002800000009000000060000000100
        200000000000D800000000000000000000000000000000000000008080000080
        8000008080000080800000808000008080000080800000808000008080000080
        8000000000000000000000000000000000000000000000000000000000000080
        8000008080000080800000000000000000000000000000000000000000000080
        8000008080000080800000808000008080000000000000000000000000000080
        8000008080000080800000808000008080000080800000808000000000000080
        8000008080000080800000808000008080000080800000808000008080000080
        800000808000008080000080800000808000}
      OnDownClick = SpinButton1DownClick
      OnUpClick = SpinButton1UpClick
    end
    object Button1: TButton
      Left = 96
      Top = 37
      Width = 75
      Height = 25
      Caption = #1079#1072#1075#1086#1083#1086#1074#1082#1080
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 224
      Top = 37
      Width = 75
      Height = 25
      Caption = #1089#1090#1088#1086#1082#1080
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 344
      Top = 37
      Width = 75
      Height = 25
      Caption = #1080#1090#1077#1084#1099
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 512
      Top = 40
      Width = 75
      Height = 25
      Caption = 'Button4'
      TabOrder = 4
      OnClick = Button4Click
    end
  end
  object Gantt1: TGantt
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 1257
    Height = 442
    ZeroDate = 44134.000000000000000000
    Headers = <
      item
        Step = 15
        Mask = 'h:nn'
      end>
    Rows = <
      item
        Text = 'HOLD '
        SubRows = 3
      end
      item
        Favorit = True
      end
      item
        Height = 20
        Text = 'grp Alpha'
        Color = 15137535
        Enabled = False
      end
      item
        Text = '1'
      end
      item
        Text = '2'
      end
      item
        Text = '3'
      end
      item
        Height = 20
        Text = 'grp Bravo'
        Color = 15531243
        Enabled = False
      end
      item
        Text = '1'
      end
      item
        Text = '2'
      end
      item
        Text = '3'
      end>
    Items = <
      item
        Tag = 0
      end
      item
        BeginValue = 10
        EndValue = 30
        Tag = 0
      end
      item
        Tag = 0
      end
      item
        RowIndex = 1
        Tag = 0
      end>
    RowHeaderWidth = 200
    Align = alClient
    Color = clWhite
    GridLineColor = clScrollBar
    PopupMenu = PopupMenu1
    ExplicitWidth = 1678
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 160
    Top = 208
    object tyjty1: TMenuItem
      Caption = 'tyjty'
    end
    object jtyjj1: TMenuItem
      Caption = 'jtyjj'
    end
  end
end
