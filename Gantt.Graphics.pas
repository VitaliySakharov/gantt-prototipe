unit Gantt.Graphics;

interface

uses Vcl.Graphics;

function GetAltColor(AColor : TColor; AValue : byte = 15): TColor;

implementation

function GetAltColor(AColor : TColor; AValue : byte = 15): TColor;
var Y, Cb, Cr : byte;
    R, G, B : byte;
    v : Integer;
begin
  //check for system color
  AColor := ColorToRGB(AColor);

  // RGB -> YCbCr
  R := AColor;
  G := AColor shr 8;
  B := AColor shr 16;

  Y := Trunc( 0.299  * R + 0.587  * G + 0.114 * B); // ����� �������
  Cb := Trunc(-0.1687 * R - 0.3313 * G + 0.5   * B + 128.0);
  Cr := Trunc( 0.5    * R - 0.4187 * G - 0.0813* B + 128.0);

  if Y > 120 then Y := Y - AValue else Y := Y + AValue;

  // YCbCr -> RGB
  v := Trunc(Y + 1.772 * (Cb - 128.0));
  if v > 255 then
    v := 255
  else if v < 0
    then v := 0;
  B := v;

  v := Trunc(Y - 0.34414 * (Cb - 128.0) - 0.71414 * (Cr - 128.0));
  if v > 255 then
    v := 255
  else if v < 0
    then v := 0;
  G := v;

  v := Trunc(Y + 1.402 * (Cr - 128.0));
  if v > 255 then
    v := 255
  else if v < 0 then
    v := 0;
  R := v;

  result := (r or (g shl 8) or (b shl 16));
end;

end.
