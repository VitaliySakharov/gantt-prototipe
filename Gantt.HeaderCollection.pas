unit Gantt.HeaderCollection;

interface

uses
  System.Classes, Vcl.Controls, Gantt.Header;

type
  TGanttHeaders = class(TCollection)
  private
    FGanttControl : TCustomControl;
    function GetItem(Index: Integer): TGanttHeader;
    procedure SetItem(Index: Integer; Value: TGanttHeader);
  protected
    function GetOwner : TPersistent; override;
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(GanttControl : TCustomControl);
    function Add : TGanttHeader;
    property Items[Index: Integer] : TGanttHeader read GetItem write SetItem; default;

    function Height() : integer;
  end;

implementation

{ TGanttHeaders }

function TGanttHeaders.Add: TGanttHeader;
begin
  Result := TGanttHeader(inherited Add);
end;

constructor TGanttHeaders.Create(GanttControl: TCustomControl);
begin
  inherited Create(TGanttHeader);
  FGanttControl := GanttControl;
end;

function TGanttHeaders.GetItem(Index: Integer): TGanttHeader;
begin
  Result := TGanttHeader(inherited GetItem(Index));
end;

function TGanttHeaders.GetOwner: TPersistent;
begin
  Result := FGanttControl;
end;

function TGanttHeaders.Height: integer;
var i : integer;
begin
  result := 0;

  for i := 0 to Count - 1 do
    result := result + Items[i].Height;
end;

procedure TGanttHeaders.SetItem(Index: Integer; Value: TGanttHeader);
begin
  inherited SetItem(Index, Value);
end;

procedure TGanttHeaders.Update(Item: TCollectionItem);
begin
  FGanttControl.Invalidate;// Repaint;
end;

end.
