unit Gantt.ItemCollection;

interface

uses
  System.Classes, Vcl.Controls, Gantt.Item;

type
TGanttItems = class(TCollection)
  private
    FParent : TCustomControl;

    function GetItem(Index: Integer): TGanttItem;
    procedure SetItem(Index: Integer; Value: TGanttItem);
  protected
    function GetOwner : TPersistent; override;
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(Control : TCustomControl);
    function Add : TGanttItem;
    property Items[Index: Integer] : TGanttItem read GetItem write SetItem; default;
  published
  end;

implementation

{ TGanttItems }

function TGanttItems.Add: TGanttItem;
begin
  Result := TGanttItem(inherited Add);
end;

constructor TGanttItems.Create(Control: TCustomControl);
begin
  inherited Create(TGanttItem);
  FParent := Control;
end;

function TGanttItems.GetItem(Index: Integer): TGanttItem;
begin
  Result := TGanttItem(inherited GetItem(Index));
end;

function TGanttItems.GetOwner: TPersistent;
begin
  Result := FParent;
end;

procedure TGanttItems.SetItem(Index: Integer; Value: TGanttItem);
begin
  inherited SetItem(Index, Value);
end;

procedure TGanttItems.Update(Item: TCollectionItem);
begin
  inherited;
  FParent.Repaint;
end;

end.
