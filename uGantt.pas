﻿unit uGantt;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.Graphics, Vcl.Forms,
  Data.DB, Math, Vcl.ExtCtrls, System.Types, Winapi.Messages, UITypes,
  uCustomGantt, Gantt.Item, Gantt.Row;

type
  TLine = class
    FValue : integer;
    FColor : TColor;
    FStyle : TPenStyle;
    FLable : String;
  end;

type
  TGantt = class;

  TSelectedItemList = class
  private
    FList : TList;
    FGanttControl : TGantt;
    function GetCount: integer;
    function Get(No: integer): TGanttItem;
  public
    constructor Create(GanttControl : TGantt);
    destructor Destroy; override;
    procedure Add(Item : TGanttItem);
    procedure Delete(Index : integer);
    procedure Clear;
    function IndexOf(Item : TGanttItem) : integer;
    function Exists(Item : TGanttItem) : boolean;
    property Item[No : integer] : TGanttItem read Get; default;
    property Count : integer read GetCount;
  end;

  TEditState = (esNone, esStretchLeft, esStretchRight, esMove, esScroll, esResize);

  TOnItemSelectedChangeEvent = procedure(Item : TGanttItem) of object;
  TOnItemChangeEvent = procedure(Item : TGanttItem; AOldBeginValue, AOldEndValue, AOldRowIndex : integer) of object;
  TOnItemPaintEvent = procedure(Item : TGanttItem; ACanvas : TCanvas) of object;
  TOnRowHeaderPaintEvent = procedure(ARowIndex : integer; ARect : TRect; ACanvas : TCanvas) of object;

  TGantt = class(TCustomGantt)
  private
    //FRows : TStrings;
    FSelected : TSelectedItemList;
    FGridLineColor: TColor;
    FSelectedColor : TColor;
    FSelectedBorderColor : TColor;
    FBuffer : TBitmap;
    FReadOnly : boolean;

    FEditState : TEditState;
    FStartEditPos : TPoint;
    FActiveRow : TGanttRow;
    FActiveItem : TGanttItem;
    FActiveRowButton : TRowHeaderButton;
    FDisableCount : integer;
    FNeedPaint : boolean;
    FLines : TStrings;

    FOnItemSelectedChange : TOnItemSelectedChangeEvent;
    FOnItemChange : TOnItemChangeEvent;
    FOnItemPaint : TOnItemPaintEvent;
    FOnRowHeaderPaint : TOnRowHeaderPaintEvent;

    procedure SetColor(const Value: TColor);
    procedure SetGridLineColor(const Value: TColor);
    function FindItem(X, Y: Integer) : TGanttItem;
    function FindRow(X, Y: Integer) : TGanttRow;
    function FindNearestRow(Y: Integer) : TGanttRow;
    function FindRowButton(X, Y : Integer) : TRowHeaderButton;
    procedure MoveItem(Item : TGanttItem; deltaX, Y : Integer; var NewBeginValue, NewEndValue, NewRowIndex : integer); overload;
    procedure MoveItem(Item : TGanttItem; deltaX, Y : Integer); overload;
    procedure Scroll(dX, X : Integer);

    procedure ItemSelectedChange(Item: TGanttItem; MultiSelect : boolean = false); dynamic;
    procedure ItemChange(Item: TGanttItem; ANewBeg, ANewEnd, ANewRow : integer); dynamic;
    procedure ItemPaint(Item: TGanttItem; ACanvas : TCanvas); dynamic;
    procedure RowHeaderPaint(Index: integer; Rect : TRect; ACanvas : TCanvas); dynamic;

    procedure Clear;
    procedure PaintHeader;
    procedure PaintRows;
    procedure PaintRow(ARow : TGanttRow);
    procedure PaintItems; overload;
    procedure PaintLines;
    procedure PaintHint(Point : TPoint; Text : array of string);
    procedure SetRows(const Value: TStrings);
    procedure SetSelectedBorderColor(const Value: TColor);
    procedure SetSelectedColor(const Value: TColor);

  protected
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    //procedure Resize; override;
  public
    procedure Paint; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DisableControls; //запрещает перерисовку
    procedure EnableControls;  //разрешает перерисовку, принудительно перерисовывает при необходимости
    property EditState : TEditState read FEditState write FEditState;
    property Selected : TSelectedItemList read FSelected write FSelected;
    procedure ScaleUp;
    procedure ScaleDown;
    function CalcHeight : integer;
    function CalcRowWidth(Text : string) : integer;
    procedure AddLine(AValue : integer; AColot : TColor; ALable : string; AStyle : TPenStyle = psSolid);
    procedure ClearLines;
  published
    property Align;
    property Font;
    property Color;
    property ShowHint;
    property ReadOnly : boolean read FReadOnly write FReadOnly default false;
    //property Rows : TStrings read FRows write SetRows;
    property GridLineColor : TColor read FGridLineColor write SetGridLineColor default cl3DLight;
    property SelectedColor : TColor read FSelectedColor write SetSelectedColor default clSkyBlue;
    property SelectedBorderColor : TColor read FSelectedBorderColor write SetSelectedBorderColor default clHighlight;
    property PopupMenu;
    property OnClick;
    property OnDblClick;
    property OnMouseMove;
    property OnMouseDown;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnResize;
    property OnItemSelectedChange: TOnItemSelectedChangeEvent read FOnItemSelectedChange write FOnItemSelectedChange;
    property OnItemChange: TOnItemChangeEvent read FOnItemChange write FOnItemChange;
    property OnItemPaint: TOnItemPaintEvent read FOnItemPaint write FOnItemPaint;
    property OnRowHeaderPaint: TOnRowHeaderPaintEvent read FOnRowHeaderPaint write FOnRowHeaderPaint;
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Samples', [TGantt]);
end;


//------------------------------------------------------------------------------
{ TGantt }
//------------------------------------------------------------------------------

procedure TGantt.AddLine(AValue: integer; AColot: TColor; ALable : string; AStyle: TPenStyle);
var Line : TLine;
begin
  Line := TLine.Create;
  Line.FValue := AValue;
  Line.FColor := AColot;
  Line.FStyle := AStyle;
  Line.FLable := ALable;

  FLines.AddObject('', Line);
end;

function TGantt.CalcHeight: integer;
var i, rowCount : integer;
begin
  rowCount := 0;

  for i := 0 to Items.Count - 1 do
    if rowCount < Items[i].RowIndex + 1 then
      rowCount := Items[i].RowIndex + 1;

  if Rows.Count > rowCount then
    rowCount := Rows.Count;

  result := Headers.Height + Rows.Height;//{TitleHeight} + 1 + rowCount * (RowHeight + 1);
end;

function TGantt.CalcRowWidth(Text : String): integer;
begin
  result := FBuffer.Canvas.TextWidth(Text);
end;

procedure TGantt.Clear;
begin
  FBuffer.Canvas.Brush.Color := Color;
  FBuffer.Canvas.FillRect(Rect(0, 0, Width, Height));
end;

procedure TGantt.ClearLines;
var i : integer;
begin
  for i := 0 to FLines.Count - 1 do
    (FLines.Objects[i] as TLine).Free;
  FLines.Clear;
end;

constructor TGantt.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  //FRows := TStringList.Create;
  FSelected := TSelectedItemList.Create(self);
  FLines := TStringList.Create;

  FBuffer := TBitmap.Create;
  //Color := clWhite;
  FGridLineColor := cl3DLight;
  FReadOnly := false;
  FEditState := esNone;

  FSelectedColor := clSkyBlue;;
  FSelectedBorderColor := clHighlight;
end;

destructor TGantt.Destroy;
var i : integer;
begin
  FBuffer.Destroy;
  //FRows.Destroy;

  for i := 0 to FLines.Count - 1 do
    (FLines.Objects[i] as TLine).Free;
  FLines.Destroy;

  inherited Destroy;
end;

procedure TGantt.DisableControls;
begin
  inc(FDisableCount);
end;

procedure TGantt.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if FStartEditPos = MousePos then
  begin
    inherited;
    Handled := false;
  end
  else
    Handled := true;
end;

procedure TGantt.EnableControls;
begin
  if FDisableCount > 0 then
    inc(FDisableCount, -1);

  if (FDisableCount = 0) and FNeedPaint then
  begin
    FNeedPaint := false;
    Paint;
  end;
end;

function TGantt.FindItem(X, Y: Integer) : TGanttItem;
var i : integer;
begin
  Result := nil;

  if (PtInRect(Rect(0, 0, Width, Headers.Height), Point(X, Y)))
  or (PtInRect(Rect(0, 0, RowHeaderWidth, Height), Point(X, Y))) then
    Exit;

  //сначала проверяем выбранные для корректной отрисовки при пересечениях
  for i := Selected.Count - 1 downto 0 do
    if PtInRect(Selected[i].ItemRect, Point(X, Y))
    or PtInRect(Selected[i].LeftSizeEditor, Point(X, Y))
    or PtInRect(Selected[i].RightSizeEditor, Point(X, Y)) then
    begin
      Result := Selected[i];
      Exit;
    end;

  //перебор всех
  for i := Items.Count - 1 downto 0 do
    if PtInRect(Items[i].ItemRect, Point(X, Y))
    or PtInRect(Items[i].LeftSizeEditor, Point(X, Y))
    or PtInRect(Items[i].RightSizeEditor, Point(X, Y)) then
    begin
      Result := Items[i];
      Break;
    end;
end;

function TGantt.FindNearestRow(Y: Integer): TGanttRow;
var i : integer;
    nearest : integer;
begin
  Result := nil;
  nearest := MaxInt;

  for i := 0 to Rows.Count - 1 do
    if Rows[i].Enabled then
      if abs(Rows[i].RowRect.Top - Y) < nearest then
      begin
        Result := Rows[i];
        nearest := abs(Rows[i].RowRect.Top - Y);
      end
end;

function TGantt.FindRow(X, Y: Integer): TGanttRow;
var i : integer;
begin
  Result := nil;

  if (PtInRect(Rect(0, 0, Width, Headers.Height), Point(X, Y))) then
    Exit;

  //find in frozen area
  if (PtInRect(Rect(0, Headers.Height, Width, Headers.Height + FrozenTop.Height), Point(X, Y))) then
  begin
    for i := 0 to FrozenTop.Count - 1 do
      if (PtInRect(FrozenTop[i].RowRect, Point(X, Y)))
      or (PtInRect(FrozenTop[i].RowHeaderRect, Point(X, Y)))then
        //if FrozenTop[i].Enabled then
        begin
          Result := FrozenTop[i];
          Break;
        end;
    Exit;
  end;

  for i := Rows.Count - 1 downto 0 do
    if (PtInRect(Rows[i].RowRect, Point(X, Y)))
    or (PtInRect(Rows[i].RowHeaderRect, Point(X, Y))) then
      //if Rows[i].Enabled then
      begin
        Result := Rows[i];
        Break;
      end;
end;

function TGantt.FindRowButton(X, Y : Integer) : TRowHeaderButton;
begin
  result := rbtNone;

  if FActiveRow <> nil then
    if ptInRect(FActiveRow.BtFavoritRect, Point(X, Y)) then
      result := rbtFavorit
    else if ptInRect(FActiveRow.BtEnabledRect, Point(X, Y)) then
      result := rbtEnabled
    else if ptInRect(FActiveRow.BtExpandedRect, Point(X, Y)) then
      result := rbtExpanded;
end;

procedure TGantt.MoveItem(Item : TGanttItem; deltaX, Y : Integer; var NewBeginValue, NewEndValue, NewRowIndex : integer);
var row : TGanttRow;
begin
  NewBeginValue := Item.BeginValue;
  NewEndValue := Item.EndValue;
  NewRowIndex := Item.RowIndex;

  with Item do
  begin
    if (EditState = esStretchLeft) then
    begin
      NewBeginValue := Item.BeginValue + Floor(deltaX / Scale);
      if NewBeginValue >= EndValue then
        NewBeginValue := EndValue -1;
    end
    else
    if (EditState = esStretchRight) then
    begin
      NewEndValue := EndValue + Floor(deltaX / Scale);
      if NewEndValue <= BeginValue then
        NewEndValue := BeginValue + 1;
    end
    else
    if EditState = esMove then
    begin
      if (eaMoveHor in EditAction) and (eaMoveVert in EditAction) then
      begin
        NewBeginValue := BeginValue + Floor(deltaX / Scale);
        NewEndValue := EndValue + Floor(deltaX / Scale);

        if (FActiveRow <> nil) and (FActiveRow.Enabled) then
          NewRowIndex := FActiveRow.Index
        else
        begin
          row := FindNearestRow(Y);
          if row <> nil then
            NewRowIndex := row.Index;
        end;
        //NewRowIndex := RowIndex + Round(abs(dY) / (RowHeight + 1)) * Sign(dY);
      end
      else if eaMoveHor in EditAction then
      begin
        NewBeginValue := BeginValue + Floor(deltaX / Scale);
        NewEndValue := EndValue + Floor(deltaX / Scale);
      end
      else
      if eaMoveVert in EditAction then
      begin
        if (FActiveRow <> nil) and (FActiveRow.Enabled) then
          NewRowIndex := FActiveRow.Index
        else
        begin
          row := FindNearestRow(Y);
          if row <> nil then NewRowIndex := row.Index;
        end;
        //NewRowIndex := RowIndex + Round(abs(dY) / (RowHeight + 1)) * Sign(dY);
      end;
    end;

    if NewRowIndex < 0 then NewRowIndex := 0;//???

    ItemRect := CalcItemPos(NewBeginValue, NewEndValue, NewRowIndex, Item.Index);
    //ItemRect := Rect(ItemRect.Left, Y, ItemRect.Right, Y + ItemRect.Height);

    {if Rows[NewRowIndex].Expandable then
      while DetectCollision(ItemRect, NewRowIndex, Item.Index) do
      begin
        Rows[NewRowIndex].ExpandCount := Rows[NewRowIndex].ExpandCount + 1;
        ItemRect := CalcItemPos(NewBeginValue, NewEndValue, NewRowIndex, Item.Index);
      end;}     
  end;  
end;

procedure TGantt.MoveItem(Item: TGanttItem; deltaX, Y : Integer);
var a, b, c : integer;
begin
  MoveItem(Item, deltaX, Y, a, b, c);
end;


procedure TGantt.ItemChange(Item: TGanttItem; ANewBeg, ANewEnd, ANewRow: integer);
var oldBeg, oldEnd, oldRow : integer;
begin
  if (ANewBeg <> Item.BeginValue)
  or (ANewEnd <> Item.EndValue)
  or (ANewRow <> Item.RowIndex) then
  begin
    oldBeg := Item.BeginValue;
    oldEnd := Item.EndValue;
    oldRow := Item.RowIndex;

    Items.BeginUpdate;
    Item.BeginValue := ANewBeg;
    Item.EndValue := ANewEnd;
    Item.RowIndex := ANewRow;
    Items.EndUpdate;

    if Assigned(FOnItemChange) then
      FOnItemChange(Item, oldBeg, oldEnd, oldRow);

    //Items.Update(Item);
  end;
end;

procedure TGantt.ItemPaint(Item: TGanttItem; ACanvas: TCanvas);
var prevRect : TRect;
    textLeft, textRight, textRow : string;
    row : TGanttRow;
    radius : integer;
begin
  if (Item <> nil) and (Item.ItemRect.IntersectsWith(Rect(0, 0, Width, Height))) then //if in borders of TGantt
  begin
    if EditState = esScroll then
    begin
      ACanvas.Brush.Color := Item.Color;;
      ACanvas.Pen.Color := Item.BorderColor;
      ACanvas.Font.Color := Item.FontColor;

      ACanvas.RoundRect(Item.ItemRect, 5, 5);
    end
    else if Selected.Exists(Item)
    and (EditState <> esNone) then
    begin
      //старое положение
      prevRect := CalcItemPos(Item.BeginValue, Item.EndValue, Item.RowIndex, Item.Index);
      ACanvas.DrawFocusRect(prevRect);

      //новое положение
      ACanvas.Brush.Color := Item.Color;;
      ACanvas.Pen.Color := Item.BorderColor;
      ACanvas.Font.Color := Item.FontColor;

      ACanvas.RoundRect(Item.ItemRect, 5, 5);

      //подсказка
      if (EditState = esStretchLeft) or (EditState = esMove) then
      begin
        textLeft := IntToStr(Floor((Item.ItemRect.Left - prevRect.Left) / Scale));
        if textLeft.Chars[0] <> '-' then textLeft := '+'+textLeft;
      end;

      if (EditState = esStretchRight) or (EditState = esMove) then
      begin
        textRight := IntToStr(Floor((Item.ItemRect.Right - prevRect.Right) / Scale));
        if textRight.Chars[0] <> '-' then textRight := '+'+textRight;
      end;

      textLeft := 'Начало: ' {+ Headers[0].IntToDateTimeFormat(ZeroDate, Item.BeginValue)} +' '+ textLeft;
      textRight := 'Конец:   ' {+ Header.IntToTimeFormat(ZeroDate, Item.EndValue)} +' '+ textRight;
      row := FindRow(Item.ItemRect.Left, Item.ItemRect.Top);
      if row <> nil then textRow := row.Text;

      {if (Rows.Count > Item.RowIndex + Floor((Item.ItemRect.Top - prevRect.Top) / (RowHeight + 1)))
      and (Item.RowIndex + Floor((Item.ItemRect.Top - prevRect.Top) / (RowHeight + 1)) >= 0) then
        textRow := Rows[Item.RowIndex + Floor((Item.ItemRect.Top - prevRect.Top) / (RowHeight + 1))].Text; }

      PaintHint(Point(Item.ItemRect.Right + 5, Item.ItemRect.Bottom + 5), [textLeft, textRight, textRow]);
    end
    else if Selected.Exists(Item) then
    begin
      ACanvas.Brush.Color := FSelectedColor;;
      ACanvas.Pen.Color := FSelectedBorderColor;
      ACanvas.Font.Color := Item.FontColor;

      if Assigned(FOnItemPaint) then
        FOnItemPaint(Item, ACanvas)
      else
        ACanvas.RoundRect(Item.ItemRect, 5, 5);
    end
    else if Item = FActiveItem then
    begin
      ACanvas.Brush.Color := GetAltColor(Item.Color);
      ACanvas.Pen.Color := GetAltColor(Item.BorderColor);
      ACanvas.Font.Color := Item.FontColor;

      if Assigned(FOnItemPaint) then
        FOnItemPaint(Item, ACanvas)
      else
        ACanvas.RoundRect(Item.ItemRect, 5, 5);
    end
    else
    begin
      ACanvas.Brush.Color := Item.Color;
      ACanvas.Pen.Color := Item.BorderColor;
      ACanvas.Font.Color := Item.FontColor;

      if Assigned(FOnItemPaint) then
        FOnItemPaint(Item, ACanvas)
      else
        ACanvas.RoundRect(Item.ItemRect, 5, 5);
    end;
  end;
end;

procedure TGantt.ItemSelectedChange(Item: TGanttItem; MultiSelect : boolean = false);
var i : integer;
    //itm : TGanttItem;
begin
  if (Item <> nil) and (MultiSelect) then
  begin
    if not Selected.Exists(Item) then
      Selected.Add(Item)
    //else
      //Selected.Delete(Selected.IndexOf(Item));
  end
  else
  begin
    {for i := Selected.Count - 1 downto 0 do
    begin
      //itm := Selected[i];
      Selected.Delete(i);

      //if Assigned(FOnItemSelectedChange) then
      //  FOnItemSelectedChange(itm);

      //Selected.Delete(i);
    end;   }
    Selected.Clear;
    if Item <> nil then
      Selected.Add(Item);
  end;
end;

procedure TGantt.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var i : integer;
begin
  inherited MouseDown(Button, Shift, X, Y);

  FStartEditPos.X := X;
  FStartEditPos.Y := Y;

  if (ssLeft in Shift) and (PtInRect(Rect(RowHeaderWidth - 5, 0, RowHeaderWidth + 5, Headers.Height), Point(X, Y))) then
  begin
    EditState := esResize;
    Exit;
  end;

  {if (ssRight in Shift) then
  begin
      EditState := esScroll;
      Exit;
  end;}
  {if (ssLeft in Shift) (PtInRect(Rect(RowHeaderWidth, 0, Width, TitleHeight), Point(X, Y))) then
  begin
    EditState := esScroll;
    Exit;
  end;}

  {if (ssRight in Shift) then
    if (PtInRect(Rect(RowHeaderWidth, 0, Width, TitleHeight), Point(X, Y)))
    or (PtInRect(Rect(RowHeaderWidth, TitleHeight, Width, Height), Point(X, Y))) then
    begin
      EditState := esScroll;
      //Exit;
    end;}


  ItemSelectedChange(FindItem(X, Y), false {(ssShift in Shift) not work with diff row height} {or (Selected.Count > 1) its work!!});
  Paint;

  if (ssLeft in Shift) {and (not ReadOnly)} then
      if not ReadOnly then
        for i := 0 to Items.Count - 1 do
          if (Selected.Exists(Items[i])) then
            if (eaStretchLeft in Items[i].EditAction) and PtInRect(Items[i].LeftSizeEditor, Point(X, Y)) then
              EditState := esStretchLeft
            else if (eaStretchRight in Items[i].EditAction) and PtInRect(Items[i].RightSizeEditor, Point(X, Y)) then
              EditState := esStretchRight
            else if ((eaMoveHor in Items[i].EditAction) or (eaMoveVert in Items[i].EditAction)) then
              EditState := esMove
            else
              EditState := esNone;
end;

procedure TGantt.MouseMove(Shift: TShiftState; X, Y: Integer);
var i : integer;
    item : TGanttItem;
    row : TGanttRow;
    bt : TRowHeaderButton;
    needPaint : boolean;
begin
  inherited MouseMove(Shift, X, Y);

  needPaint := false;

  row := FindRow(X, Y);
  if row <> FActiveRow then
  begin
    FActiveRow := row;
    needPaint := true;
    //Paint;
  end;

  if (ssRight in Shift) {and (EditState = esScroll)} then //двигаем сетку
  begin
    //EditState := esScroll;
    Scroll(X - FStartEditPos.X, X);
  end
  else if (ssLeft in Shift) and (EditState = esResize) then //растягиваем заголовок строки
  begin
    RowHeaderWidth := RowHeaderWidth + X - FStartEditPos.X;
    FStartEditPos.X := X;
    needPaint := true;
    //Paint;
  end
  else if (ssLeft in Shift) and (EditState in [esMove, esStretchLeft, esStretchRight]) then //редактируется
  begin
    for i := 0 to Selected.Count - 1 do
      MoveItem(Selected[i], X - FStartEditPos.X, Y {- FStartEditPos.Y});
    needPaint := true;
    //Paint;
  end
  else
  begin //ищем под курсором
    item := FindItem(X, Y);
    if item <> FActiveItem then
    begin
      FActiveItem := item;
      needPaint := true;
      //Paint;
    end;

    bt := FindRowButton(X, Y);
    if bt <> FActiveRowButton then
    begin
      FActiveRowButton := bt;
      needPaint := true;
    end;
  end;

  if needPaint then Paint;

  if (Y <= Headers.Height) and (X > RowHeaderWidth - 5) and (X < RowHeaderWidth + 5) then
    Cursor := crHSplit
  else if (FActiveItem <> nil) and (not ReadOnly) then
    if ((eaStretchLeft in FActiveItem.EditAction) and PtInRect(FActiveItem.LeftSizeEditor, Point(X, Y)))
    or ((eaStretchRight in FActiveItem.EditAction) and PtInRect(FActiveItem.RightSizeEditor, Point(X, Y))) then
      Cursor := crSizeWE
    else
      Cursor := crDefault
  else
    Cursor := crDefault;
end;

procedure TGantt.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var i : integer;
    ANewBegin, ANewEnd, ANewRowIndex : integer;
begin
  //inherited MouseUp(Button, Shift, X, Y);

  if (EditState in [esStretchLeft, esStretchRight, esMove]) then
    for i := 0 to Selected.Count - 1 do
    begin
      if Selected.Count > i then
      begin
        MoveItem(Selected[i], X - FStartEditPos.X, Y {- FStartEditPos.Y}, ANewBegin, ANewEnd, ANewRowIndex);
        if ANewRowIndex < 0 then ANewRowIndex := 0;//???

        ItemChange(Selected[i], ANewBegin, ANewEnd, ANewRowIndex);
      end;
    end;

  FEditState := esNone;
  FStartEditPos.X := 0;
  FStartEditPos.Y := 0;
  Paint;

  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TGantt.Paint;
begin
  //inherited;
  if FDisableCount > 0 then
  begin
    FNeedPaint := true;
    Exit;
  end;

  FBuffer.Width := Width;
  FBuffer.Height := Height;

  //Header.Canvas.

  //clear areas
  FBuffer.Canvas.Brush.Color := Color;
  FBuffer.Canvas.FillRect(Rect(0, 0, Width, Headers.Height + 1));
  FBuffer.Canvas.FillRect(Rect(0, Headers.Height + 1, Width - VertScroll.Width - 2, Height));

  PaintHeader;

  PaintRows;

  //PaintGridHeader;
  //PaintLines;
  PaintItems;



  //Canvas.CopyRect(Rect(0, 0, Width, Headers.Height + 1), FBuffer.Canvas, Rect(0, 0, Width, Headers.Height + 1));
  //Canvas.CopyRect(Rect(RowHeaderWidth + 1, Headers.Height + 1, Width - VertScroll.Width - 2, Height), FBuffer.Canvas, Rect(RowHeaderWidth + 1, Headers.Height + 1, Width - VertScroll.Width - 2, Height));

  inc(FCnt);
  Canvas.Font.Color := clBlack;
  Canvas.TextOut(Width - 30, Height - 15, FCnt.ToString());
  Canvas.TextOut(Width - 100, Height - 15, FCntR.ToString());
  Canvas.TextOut(Width - 150, Height - 15, FCntI.ToString());
end;

procedure TGantt.PaintHeader;
var k, i : integer;
    GridFirstStep : integer;
    x, y : integer;
    text : string;
    textRect : TRect;
begin
  //clear area
  //FBuffer.Canvas.Brush.Color := Color;
  //FBuffer.Canvas.FillRect(Rect(0, 0, Width, Headers.Height));

  //top border
  //FBuffer.Canvas.MoveTo(0, 0);
  //FBuffer.Canvas.MoveTo(0, Width);

  y := 0;

  for i := 0 to Headers.Count - 1 do
  begin
    FBuffer.Canvas.Pen.Color := Headers[i].BorderColor;
    FBuffer.Canvas.Brush.Color := Headers[i].Color;
    FBuffer.Canvas.Font.Color := Headers[i].FontColor;

    FBuffer.Canvas.FillRect(Rect(0, y, Width, y + Headers[i].Height));

    k := 0;
    while ((Min + k) mod Headers[i].Step) <> 0 do
      inc(k);
    GridFirstStep := k;

    k := 0;
    while (GridFirstStep * Scale + Headers[i].Step * k * Scale < (Width - RowHeaderWidth)) do
    begin
      x := RowHeaderWidth + GridFirstStep * Scale + Headers[i].Step * Scale * k;

      FBuffer.Canvas.MoveTo(x, y);
      FBuffer.Canvas.LineTo(x, y + Headers[i].Height);

      text := Headers[i].IntToDateTimeFormat(ZeroDate, Min + GridFirstStep + Headers[i].Step * k);
      textRect := Rect(x + 2, y, x + Headers[i].Step * Scale, y + Headers[i].Height);
      FBuffer.Canvas.TextRect(textRect, text, [tfVerticalCenter, tfSingleLine]);

      inc(k);
    end;

    //underline
    FBuffer.Canvas.MoveTo(RowHeaderWidth, y + Headers[i].Height - 1);
    FBuffer.Canvas.LineTo(Width, y + Headers[i].Height - 1);

    y := y + Headers[i].Height;
  end;

  //horizontal border
  FBuffer.Canvas.Pen.Color := GridLineColor;

  FBuffer.Canvas.MoveTo(0, 0);
  FBuffer.Canvas.LineTo(Width, 0);

  FBuffer.Canvas.MoveTo(0, Headers.Height - 1);
  FBuffer.Canvas.LineTo(Width, Headers.Height - 1);
  //vertical border
  FBuffer.Canvas.MoveTo(0, 0);
  FBuffer.Canvas.LineTo(0, Headers.Height - 1);

  FBuffer.Canvas.MoveTo(RowHeaderWidth, 0);
  FBuffer.Canvas.LineTo(RowHeaderWidth, Headers.Height - 1);

  FBuffer.Canvas.MoveTo(Width, 0);
  FBuffer.Canvas.LineTo(Width, Headers.Height - 1);

  Canvas.CopyRect(Rect(0, 0, Width, Headers.Height), FBuffer.Canvas, Rect(0, 0, Width, Headers.Height));
  //Canvas.CopyRect(Rect(0, 0, Width, Headers.Height + 3), FBuffer.Canvas, Rect(0, 0, Width, Headers.Height + 3));
end;

procedure TGantt.PaintHint(Point : TPoint; Text : array of string);
var HintRect : TRect;
    i : integer;
begin
  FBuffer.Canvas.Brush.Color := clInfoBk;
  FBuffer.Canvas.Font.Color := clInfoText;
  FBuffer.Canvas.Pen.Color := clInfoText;
  FBuffer.Canvas.Pen.Style := psSolid;

  HintRect.Left := Point.X;
  HintRect.Top := Point.Y;
  HintRect.Width := 10;
  for i := 0 to Length(Text) - 1 do
    if HintRect.Width < 5 + FBuffer.Canvas.TextWidth(Text[i]) + 5 then
      HintRect.Width := 5 + FBuffer.Canvas.TextWidth(Text[i]) + 5;
  HintRect.Height := 5 + (FBuffer.Canvas.TextHeight('sde') + 5) * Length(Text);

  FBuffer.Canvas.Rectangle(HintRect);

  for i := 0 to Length(Text) - 1 do
    FBuffer.Canvas.TextOut(HintRect.Left + 5, HintRect.Top + 5 + (FBuffer.Canvas.TextHeight(Text[i]) + 5) * i, Text[i]);
end;

procedure TGantt.PaintItems;
var i, r : integer;
begin
  //frozen top area
  for r := 0 to FrozenTop.Count - 1 do
  begin
    for i := 0 to Items.Count - 1 do
      if Items[i].RowIndex = FrozenTop[r].Index then
        if IntersectRect(FrozenTop[r].RowRect, Items[i].ItemRect) then
          if not Selected.Exists(Items[i]) then
            ItemPaint(Items[i], FBuffer.Canvas);

    //выбранные поверх всех
    for i := 0 to Selected.Count - 1 do
      if Selected[i].RowIndex = FrozenTop[r].Index then
        if IntersectRect(FrozenTop[r].RowRect, Selected[i].ItemRect) then
          ItemPaint(Selected[i], FBuffer.Canvas);
  end;

  Canvas.CopyRect(Rect(RowHeaderWidth, Headers.Height, Width - VertScroll.Width - 2, Headers.Height + FrozenTop.Height),
                  FBuffer.Canvas,
                  Rect(RowHeaderWidth, Headers.Height, Width - VertScroll.Width - 2, Headers.Height + FrozenTop.Height));


  for i := 0 to Items.Count - 1 do
    if not Selected.Exists(Items[i]) then
      //if IntersectRect(Rect(RowHeaderWidth, Headers.Height + FrozenTop.Height, Width - VertScroll.Width - 2, Height), Items[i].ItemRect) then
        ItemPaint(Items[i], FBuffer.Canvas);

  //выбранные поверх всех
  for i := 0 to Selected.Count - 1 do
    //if IntersectRect(Rect(RowHeaderWidth, Headers.Height + FrozenTop.Height, Width - VertScroll.Width - 2, Height), Items[i].ItemRect) then
      ItemPaint(Selected[i], FBuffer.Canvas);

  Canvas.CopyRect(Rect(RowHeaderWidth, Headers.Height + FrozenTop.Height, Width - VertScroll.Width - 2, Height), FBuffer.Canvas, Rect(RowHeaderWidth, Headers.Height + FrozenTop.Height, Width - VertScroll.Width - 2, Height));
  //всплывающая подсказка
  {if (FActiveItem <> nil)
    and (FEditState in [esNone]) then
    PaintHint(Point(FActiveItem.ItemRect.Right + 10, FActiveItem.ItemRect.Bottom + 10),
    ['', '', '',
    '-----------------','', 'Время  10:00 - 24.18', '']);   }
end;

procedure TGantt.PaintLines;
var i, d : integer;
    Line : TLine;
begin
  d := Scale;
  if d = 0 then Exit;

  for i := 0 to FLines.Count - 1 do
    if (FLines.Objects[i] <> nil) and (FLines.Objects[i] is TLine) then
    begin
      Line := (FLines.Objects[i] as TLine);
      //if (Line.FValue >= Min) and (Line.FValue <= Max) then
      begin
        FBuffer.Canvas.Pen.Color := Line.FColor;
        FBuffer.Canvas.Pen.Style := Line.FStyle;
        FBuffer.Canvas.Font.Color := Line.FColor;
        FBuffer.Canvas.Brush.Color := Color;
        FBuffer.Canvas.MoveTo(RowHeaderWidth + (Line.FValue - Min) * d, 0);
        FBuffer.Canvas.LineTo(RowHeaderWidth + (Line.FValue - Min) * d, Height);

        if Line.FLable <> '' then
          FBuffer.Canvas.TextOut(RowHeaderWidth + (Line.FValue - Min) * d + 3, Round((Headers.Height) / 2), Line.FLable);
      end;
    end;
end;

procedure TGantt.PaintRow(ARow: TGanttRow);
var al : TTextFormats;
    favorRect, enabledRect, expRect, textRect : TRect;
    var text : string;
begin
  if (ARow = FActiveRow) then
    FBuffer.Canvas.Brush.Color := GetAltColor(ARow.Color, 10)
  else
    FBuffer.Canvas.Brush.Color := ARow.Color;

  FBuffer.Canvas.Pen.Color := ARow.BorderColor;
  FBuffer.Canvas.Pen.Style := psSolid;
  FBuffer.Canvas.Font.Color := ARow.FontColor;

  //fill row header
  FBuffer.Canvas.FillRect(ARow.RowHeaderRect);


  //paint Favorit
  favorRect := ARow.BtFavoritRect;// Rect(ARow.RowHeaderRect.Left, ARow.RowHeaderRect.Top, ARow.RowHeaderRect.Left + 18, ARow.RowHeaderRect.Bottom);
  text := '★';
  FBuffer.Canvas.Font.Size := FBuffer.Canvas.Font.Size + 6;

  if ARow.Favorit then
    FBuffer.Canvas.Font.Color := clHighlight
  else
    FBuffer.Canvas.Font.Color := GetAltColor(FBuffer.Canvas.Brush.Color, 25);

  if (FActiveRow = ARow) and (FActiveRowButton = rbtFavorit) then
    FBuffer.Canvas.Font.Color := GetAltColor(FBuffer.Canvas.Font.Color, 50);

  FBuffer.Canvas.TextRect(favorRect, text, [tfCenter, tfVerticalCenter, tfSingleLine]);
  FBuffer.Canvas.Font.Size := Font.Size;
  FBuffer.Canvas.Font.Color := ARow.FontColor;


  //paint Enabled
  enabledRect := ARow.BtEnabledRect; //Rect(favorRect.Right, ARow.RowHeaderRect.Top, favorRect.Right + 18, ARow.RowHeaderRect.Bottom);
  text := '🔒';

  if not ARow.Enabled then
    FBuffer.Canvas.Font.Color := clMedGray
  else
    FBuffer.Canvas.Font.Color := GetAltColor(FBuffer.Canvas.Brush.Color, 25);

  if (FActiveRow = ARow) and (FActiveRowButton = rbtEnabled) then
    FBuffer.Canvas.Font.Color := GetAltColor(FBuffer.Canvas.Font.Color, 50);

  FBuffer.Canvas.TextRect(enabledRect, text, [tfCenter, tfVerticalCenter, tfSingleLine]);
  FBuffer.Canvas.Font.Color := ARow.FontColor;


  //paint Expanded
  expRect := ARow.BtExpandedRect; //Rect(favorRect.Right, ARow.RowHeaderRect.Top, favorRect.Right + 18, ARow.RowHeaderRect.Bottom);
  FBuffer.Canvas.Font.Size := FBuffer.Canvas.Font.Size + 4;

  if ARow.SubRows <= 1 then
    text := ''
  else if ARow.Expanded then
  begin
    //FBuffer.Canvas.Font.Color := clMedGray;
    text := '🞃';
  end
  else
  begin
    //FBuffer.Canvas.Font.Color := GetAltColor(FBuffer.Canvas.Brush.Color, 25);
    text := '🞂';
  end;
  FBuffer.Canvas.Font.Color := clMedGray;

  if (FActiveRow = ARow) and (FActiveRowButton = rbtExpanded) then
    FBuffer.Canvas.Font.Color := GetAltColor(FBuffer.Canvas.Font.Color, 50);

  FBuffer.Canvas.TextRect(expRect, text, [tfCenter, tfVerticalCenter, tfSingleLine]);
  FBuffer.Canvas.Font.Color := ARow.FontColor;
  FBuffer.Canvas.Font.Size := Font.Size;

  //paint text
  textRect := Rect(enabledRect.Right + 3, ARow.RowHeaderRect.Top, expRect.Left - 1, ARow.RowHeaderRect.Bottom - 1);

  if Assigned(FOnRowHeaderPaint) then
    FOnRowHeaderPaint(ARow.Index, textRect, FBuffer.Canvas)
  else
  begin
    text := ARow.Text;
    if ARow.TextAlignment = taLeftJustify then al := tfLeft
    else if ARow.TextAlignment = taRightJustify then al := tfRight
    else al := tfCenter;

    FBuffer.Canvas.TextRect(textRect, text, [al, tfVerticalCenter, tfSingleLine, tfEndEllipsis]);
  end;

  //fill row
  FBuffer.Canvas.FillRect(ARow.RowRect);

  //underline
  if (FrozenTop.Height > 0) and (ARow.RowRect.Bottom = Headers.Height + FrozenTop.Height) then
    FBuffer.Canvas.Pen.Color := GridLineColor;

  FBuffer.Canvas.MoveTo(0, ARow.RowRect.Bottom - 1);
  FBuffer.Canvas.LineTo(ARow.RowRect.Right, ARow.RowRect.Bottom - 1);

  //left border
  FBuffer.Canvas.Pen.Color := GridLineColor;
  FBuffer.Canvas.MoveTo(0, ARow.RowRect.Top);
  FBuffer.Canvas.LineTo(0, ARow.RowRect.Bottom);

  //delimeter line
  FBuffer.Canvas.MoveTo(ARow.RowHeaderRect.Right, ARow.RowRect.Top);
  FBuffer.Canvas.LineTo(ARow.RowHeaderRect.Right, ARow.RowRect.Bottom);
end;

procedure TGantt.PaintRows;
var i : integer;
begin
  //paint rows in common area
  for i := 0 to Rows.Count - 1 do
    if not FrozenTop.Exists(Rows[i]) then
      Rows[i].Paint(FBuffer.Canvas, Rows[i] = FActiveRow, FActiveRowButton, GridLineColor);
      //PaintRow(Rows[i]);

  //paint rows in frozen area
  for i := 0 to FrozenTop.Count - 1 do
    //PaintRow(FrozenTop[i]);
    FrozenTop[i].Paint(FBuffer.Canvas, FrozenTop[i] = FActiveRow, FActiveRowButton, GridLineColor);

  //row header
  Canvas.CopyRect(Rect(0, Headers.Height, RowHeaderWidth, Height), FBuffer.Canvas, Rect(0, Headers.Height, RowHeaderWidth, Height));
end;

procedure TGantt.RowHeaderPaint(Index: integer; Rect: TRect; ACanvas: TCanvas);
begin
  if Assigned(FOnRowHeaderPaint) then
    FOnRowHeaderPaint(Index, Rect, ACanvas);
end;

procedure TGantt.ScaleDown;
var d : integer;
begin
  if Scale > 1 then
  begin
    d := Scale - 1;
    Max := Floor(Min + (Width - RowHeaderWidth) / d);
  end;
end;

procedure TGantt.ScaleUp;
var d : integer;
begin
  if Scale < 15 then
  begin
    d := Scale + 1;
    Max := Floor(Min + (Width - RowHeaderWidth) / d);
  end;
end;

procedure TGantt.Scroll(dX, X: Integer);
var val : integer;
begin
  val := dX div Scale;

  if val <> 0 then
  begin
    DisableControls;
    Min := Min - val;
    FStartEditPos.X := X;
    EnableControls;
  end;
end;

procedure TGantt.SetColor(const Value: TColor);
begin
  //FGridBackgroundColor := Value;
  Paint;
end;

procedure TGantt.SetGridLineColor(const Value: TColor);
begin
  FGridLineColor := Value;
  Paint;
end;

procedure TGantt.SetRows(const Value: TStrings);
begin

end;

{procedure TGantt.SetRows(const Value: TStrings);
begin
  if Assigned(FRows) then
    FRows.Assign(Value)
  else
    FRows := Value;

  Paint;
end;  }

procedure TGantt.SetSelectedBorderColor(const Value: TColor);
begin
  FSelectedBorderColor := Value;
  Paint;
end;

procedure TGantt.SetSelectedColor(const Value: TColor);
begin
  FSelectedColor := Value;
  Paint;
end;




{ TSelectedItemList }

procedure TSelectedItemList.Add(Item: TGanttItem);
begin
  FList.Add(Item);

  if Assigned(FGanttControl.FOnItemSelectedChange) then
    FGanttControl.FOnItemSelectedChange(Item);

  FGanttControl.Paint;
end;

procedure TSelectedItemList.Clear;
begin
  if FList.Count > 0 then
  begin
    FList.Clear;

    if Assigned(FGanttControl.FOnItemSelectedChange) then
      FGanttControl.FOnItemSelectedChange(nil);

    FGanttControl.Paint;
  end;
end;

constructor TSelectedItemList.Create(GanttControl: TGantt);
begin
  FList := TList.Create();
  FGanttControl := GanttControl;
end;

procedure TSelectedItemList.Delete(Index: integer);
var item : TGanttItem;
begin
  item := FList[Index];
  FList.Delete(Index);

  if Assigned(FGanttControl.FOnItemSelectedChange) then
    FGanttControl.FOnItemSelectedChange(item);
end;

destructor TSelectedItemList.Destroy;
begin
  FList.Destroy;
  inherited;
end;

function TSelectedItemList.Exists(Item: TGanttItem): boolean;
begin
  Result := FList.IndexOf(Item) >= 0;
end;

function TSelectedItemList.Get(No: integer): TGanttItem;
begin
  Result := TGanttItem(FList[No]);
end;

function TSelectedItemList.GetCount: integer;
begin
  Result := FList.Count;
end;

function TSelectedItemList.IndexOf(Item: TGanttItem): integer;
begin
  Result := FList.IndexOf(Item);
end;


end.
