unit Gantt.Frozen;

interface

uses
  System.Classes, System.Types, Gantt.Row;

type

  TGanttFrozen = class
  private
    FOwner : TComponent;
    FList : TList;
    function GetCount: integer;
    function Get(No: integer): TGanttRow;
  public
    constructor Create(Owner : TComponent);
    destructor Destroy; override;
    procedure Add(Item : TGanttRow);
    procedure Delete(Index : integer);
    procedure Clear;
    //function IndexOf(Item : TGanttRow) : integer;
    function Exists(Item : TGanttRow) : boolean;
    property Item[No : integer] : TGanttRow read Get; default;
    property Count : integer read GetCount;
    function Height : integer;
  end;

implementation

{ TGanttFrozen }

procedure TGanttFrozen.Add(Item: TGanttRow);
begin
  FList.Add(Item);
end;

procedure TGanttFrozen.Clear;
begin
  if FList.Count > 0 then
    FList.Clear;
end;

constructor TGanttFrozen.Create(Owner: TComponent);
begin
  FList := TList.Create();
  FOwner := Owner;
end;

procedure TGanttFrozen.Delete(Index: integer);
var item : TGanttRow;
begin
  item := FList[Index];
  FList.Delete(Index);
end;

destructor TGanttFrozen.Destroy;
begin
  FList.Destroy;
  inherited;
end;

function TGanttFrozen.Exists(Item: TGanttRow): boolean;
begin
  Result := FList.IndexOf(Item) >= 0;
end;

function TGanttFrozen.Get(No: integer): TGanttRow;
begin
  Result := TGanttRow(FList[No]);
end;

function TGanttFrozen.GetCount: integer;
begin
  result := FList.Count;
end;

function TGanttFrozen.Height: integer;
var i : integer;
begin
  result := 0;

  for i := 0 to FList.Count - 1 do
    //result := result + Get(i).Height * Get(i).SubRows;
    if Get(i).Expanded then
      result := result + Get(i).Height * Get(i).SubRows
    else
      result := result + Get(i).Height;
end;

end.
