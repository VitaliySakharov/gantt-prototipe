unit Gantt.RowCollection;

interface

uses
  System.Classes, Vcl.Controls, Gantt.Row;

type
  TGanttRows = class(TCollection)
  private
    FGanttControl : TCustomControl;

    function GetItem(Index: Integer): TGanttRow;
    procedure SetItem(Index: Integer; Value: TGanttRow);
  protected
    function GetOwner : TPersistent; override;
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(GanttControl : TCustomControl);
    function Add : TGanttRow;
    property Items[Index: Integer] : TGanttRow read GetItem write SetItem; default;

    function Height() : integer;
    function FavoritCount() : integer;
  end;

implementation

{ TGanttRows }

function TGanttRows.Add: TGanttRow;
begin
  Result := TGanttRow(inherited Add);
end;

constructor TGanttRows.Create(GanttControl: TCustomControl);
begin
  inherited Create(TGanttRow);
  FGanttControl := GanttControl;
end;

function TGanttRows.FavoritCount: integer;
var i : integer;
begin
  result := 0;

  for i := 0 to Count - 1 do
    if Items[i].Favorit then
      inc(result);
end;

function TGanttRows.GetItem(Index: Integer): TGanttRow;
begin
  Result := TGanttRow(inherited GetItem(Index));
end;

function TGanttRows.GetOwner: TPersistent;
begin
  Result := FGanttControl;
end;

function TGanttRows.Height: integer;
var i : integer;
begin
  result := 0;

  for i := 0 to Count - 1 do
    if Items[i].Expanded then
      result := result + Items[i].Height * Items[i].SubRows
    else
      result := result + Items[i].Height;
end;

procedure TGanttRows.SetItem(Index: Integer; Value: TGanttRow);
begin
  inherited SetItem(Index, Value);
end;

procedure TGanttRows.Update(Item: TCollectionItem);
begin
  inherited;
  FGanttControl.Repaint;
end;

end.
