unit Gantt.Item;

interface

uses
  System.Classes, System.Types, Vcl.Graphics;

type
  //TGanttItems = class;
  TEditAction = set of (eaStretchLeft, eaStretchRight, eaMoveHor, eaMoveVert);

  TGanttItem = class(TCollectionItem)
  private
    FBeginValue : integer;
    FEndValue : integer;
    FRowIndex : integer;
    FText : string;
    FTag : integer;
    FObject : TObject;
    FEditAction : TEditAction;
    //���������� ��������
    FItemRect : TRect;
    FLeftSizeEditor : TRect;
    FRightSizeEditor : TRect;
    //������� �����
    FFontColor : TColor;
    FColor : TColor;
    FBorderColor : TColor;

    procedure SetRowIndex(const Value: integer);
    procedure SetText(const Value: string);
    procedure SetBeginValue(const Value: integer);
    procedure SetEndValue(const Value: integer);
    procedure SetLeftRightEditors(const value : TRect);
  protected
    function GetDisplayName: string; override;
  public
    constructor Create(Collection: TCollection); override;
    property ItemRect : TRect read FItemRect write SetLeftRightEditors;
    property LeftSizeEditor : TRect read FLeftSizeEditor;
    property RightSizeEditor : TRect read FRightSizeEditor;
    property FontColor : TColor read FFontColor write FFontColor;
    property Color : TColor read FColor write FColor;
    property BorderColor : TColor read FBorderColor write FBorderColor;
    property DataObj : TObject read FObject write FObject;
  published
    property BeginValue : integer read FBeginValue write SetBeginValue default 5;
    property EndValue : integer read FEndValue write SetEndValue default 20;
    property RowIndex : integer read FRowIndex write SetRowIndex default 0;
    property Text : string read FText write SetText;
    property Tag : integer read FTag write FTag;
    property EditAction : TEditAction read FEditAction write FEditAction default [eaStretchLeft, eaStretchRight, eaMoveHor, eaMoveVert];
  end;

implementation

//------------------------------------------------------------------------------
{ TGanttItem }
//------------------------------------------------------------------------------

constructor TGanttItem.Create(Collection: TCollection);
begin
  FRowIndex := 0;//Collection.Count;
  FBeginValue := 5;
  FEndValue := 20;
  //FFont := TFont.Create;
  FFontColor := clWhite;
  FColor := $00B48246;
  FBorderColor := clNavy;
  EditAction := [eaStretchLeft, eaStretchRight, eaMoveHor, eaMoveVert];

  inherited Create(Collection); //constructor last because it automatically calls the parant method TCollection.Update
end;

procedure TGanttItem.SetRowIndex(const Value: integer);
begin
  FRowIndex := Value;
  Changed(false);
end;

procedure TGanttItem.SetText(const Value: string);
begin
  FText := Value;
  Changed(false);
end;

procedure TGanttItem.SetBeginValue(const Value: integer);
begin
  FBeginValue := Value;
  Changed(false);
end;

procedure TGanttItem.SetEndValue(const Value: integer);
begin
  FEndValue := Value;
  Changed(false);
end;

procedure TGanttItem.SetLeftRightEditors(const value: TRect);
var ax, ay, bx, by : integer;
begin
  FItemRect := value;

  ax := value.Left;
  ay := value.Top;
  bx := value.Right;
  by := value.Bottom;

  FLeftSizeEditor := Rect(ax - 3, ay + Round((by - ay) / 2) - 6, ax + 3, ay + Round((by - ay) / 2) + 6);
  FRightSizeEditor := Rect(bx - 3, ay + Round((by - ay) / 2) - 6, bx + 3, ay + Round((by - ay) / 2) + 6);
end;

function TGanttItem.GetDisplayName: string;
begin
  Result := Text;
  if Result = '' then Result := inherited GetDisplayName;
end;

end.
