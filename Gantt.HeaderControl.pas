unit Gantt.HeaderControl;

interface

uses
  System.Classes, Vcl.Controls, Vcl.Graphics, Gantt.Header, Gantt.HeaderCollection;

type
  TGanttHeaderControl = class(TCustomControl)
  private

  public
    constructor Create(AOwner: TComponent); override;
    procedure Paint(Headers : TGanttHeaders; Scale, Min, RowHeaderWidth : integer; ZeroDate : TDateTime);
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property PopupMenu;
  end;

implementation

{ TGanttHeaderControl }

constructor TGanttHeaderControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Align := alTop;
  Color := clWindowText;
end;

procedure TGanttHeaderControl.Paint(Headers : TGanttHeaders; Scale, Min, RowHeaderWidth : integer; ZeroDate : TDateTime);
var k, i : integer;
    GridFirstStep : integer;
    x, y : integer;
begin
  //clear area
  //FBuffer.Canvas.Brush.Color := Color;
  //FBuffer.Canvas.FillRect(Rect(0, 0, Width, Headers.Height));

  y := 0;

  for i := 0 to Headers.Count - 1 do
  begin
    Canvas.Pen.Color := Headers[i].BorderColor;
    Canvas.Brush.Color := Headers[i].Color;
    Canvas.Font.Color := Headers[i].FontColor;

    Canvas.Rectangle(0, y, RowHeaderWidth + 1, y + Headers[i].Height + 1);
    Canvas.Rectangle(RowHeaderWidth, y, Width, y + Headers[i].Height + 1);

    k := 0;
    while ((Min + k) mod Headers[i].Step) <> 0 do
      inc(k);
    GridFirstStep := k;

    k := 0;
    while (GridFirstStep * Scale + Headers[i].Step * k * Scale < (Width - RowHeaderWidth)) do
    begin
      x := RowHeaderWidth + GridFirstStep * Scale + Headers[i].Step * Scale * k;

      Canvas.MoveTo(x, y);
      Canvas.LineTo(x, y + Headers[i].Height + 1);
      Canvas.TextRect(Rect(x + 1, y + 1, x + RowHeaderWidth, y + Headers[i].Height), x + 2, y,  Headers[i].IntToDateTimeFormat(ZeroDate, Min + GridFirstStep + Headers[i].Step * k));

      inc(k);
    end;

    y := y + Headers[i].Height;
  end;

  inherited Paint;
end;

end.
