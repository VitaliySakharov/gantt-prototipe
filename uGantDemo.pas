unit uGantDemo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Samples.Spin, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Grids,
  uCustomGantt, uGantt, Gantt.Header, Gantt.Item, Gantt.Row, Vcl.Menus;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    SpinButton1: TSpinButton;
    Button1: TButton;
    Button2: TButton;
    Gantt1: TGantt;
    Button3: TButton;
    Button4: TButton;
    PopupMenu1: TPopupMenu;
    tyjty1: TMenuItem;
    jtyjj1: TMenuItem;
    procedure SpinButton1UpClick(Sender: TObject);
    procedure SpinButton1DownClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  Gantt1.Headers.Add;
end;

procedure TForm1.Button2Click(Sender: TObject);
var i : integer;
    row : TGanttRow;
begin
  Gantt1.DisableControls;

  for i := 0 to 30 do
  begin
    row := Gantt1.Rows.Add();
    row.Text := '������ ' + intToStr(row.Index);
    //row.Height := 20;
    row.Favorit := Random(5) = 1;
    row.Enabled := not (Random(5) = 1);
  end;
  Gantt1.EnableControls;
end;

procedure TForm1.Button3Click(Sender: TObject);
var i : integer;
    item : TGanttItem;
begin
  Gantt1.DisableControls;

  for i := 0 to 10 do
  begin
    item := Gantt1.Items.Add();
    item.RowIndex := Random(Gantt1.Rows.Count);
    item.BeginValue := Random(250);
    item.EndValue := item.BeginValue + 20 + Random(30);
  end;
  Gantt1.EnableControls;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Gantt1.rows[0].Expanded := not Gantt1.rows[0].Expanded;
end;

procedure TForm1.PopupMenu1Popup(Sender: TObject);
begin
  if PopupMenu1.PopupComponent = Gantt1 then
    PopupMenu1.CloseMenu;
end;

procedure TForm1.SpinButton1DownClick(Sender: TObject);
begin
  //Gantt1.Header.Height := Gantt1.Header.Height - 1;
end;

procedure TForm1.SpinButton1UpClick(Sender: TObject);
begin
  //Gantt1.Header.Height := Gantt1.Header.Height + 1;
end;

end.
