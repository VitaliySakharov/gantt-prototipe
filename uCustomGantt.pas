unit uCustomGantt;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.Graphics, Vcl.Forms,
  Data.DB, Math, Vcl.ExtCtrls, System.Types, Winapi.Messages, Vcl.StdCtrls,
  Gantt.Item, Gantt.Header, Gantt.HeaderCollection, Gantt.RowCollection,
  Gantt.Frozen, Gantt.Row;

type
  TCustomGantt = class;

  TGanttItems = class(TCollection)
  private
    FGanttControl : TCustomGantt;
    function GetItem(Index: Integer): TGanttItem;
    procedure SetItem(Index: Integer; Value: TGanttItem);
  protected
    function GetOwner : TPersistent; override;
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(GanttControl : TCustomGantt);
    function Add : TGanttItem;
    property Items[Index: Integer] : TGanttItem read GetItem write SetItem; default;
  published
  end;

  TCustomGantt = class(TCustomControl)
  private
    FMin : integer;
    FMax : integer;
    FScale : integer;
    FZeroDate : TDate;

    FHeaders : TGanttHeaders;
    FRows : TGanttRows;
    FItems : TGanttItems;

    FRowHeaderWidth : integer;
    //FRowHeight : integer;
    FVertScroll : TScrollBar;
    FFrozenTop : TGanttFrozen;
    //procedure SetHeaders(const Value: TGanttHeaders);
    procedure SetItems(const Value: TGanttItems);
    procedure SetMax(const Value: integer);
    procedure SetMin(const Value: integer);
    procedure SetRowHeaderWidth(const Value: integer);
    procedure SetScale(const Value: integer);
    //procedure SetRowHeight(const Value: integer);
  protected
    FCnt : integer;
    FCntR : integer;
    FCntI : integer;

    property VertScroll : TScrollBar read FVertScroll;
    property FrozenTop : TGanttFrozen read FFrozenTop;

    procedure Scroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure ScrollHor(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Invalidate; override;

    //function GetScale : integer;
    function GetAltColor(AColor : TColor; AValue : byte = 15) : TColor;
    function DetectCollision(AItemIndex : integer; ANewRect : TRect) : boolean;
    function CalcItemPos(ABeg, AEnd, ARowIndex, AItemIndex : integer) : TRect; overload;
    procedure CalcItemPos(Item : TGanttItem); overload; virtual;
    procedure CalcItemPos; overload; virtual;

    procedure CalcRowPos(); overload;
  published
    property Min : integer read FMin Write SetMin default 0;
    property Max : integer read FMax Write SetMax default 60;
    property Scale : integer read FScale Write SetScale default 5;
    property ZeroDate : TDate read FZeroDate write FZeroDate;
    property Headers : TGanttHeaders read FHeaders write FHeaders;//SetHeaders;
    property Rows : TGanttRows read FRows write FRows;
    property Items : TGanttItems read FItems write SetItems;

    property RowHeaderWidth : integer read FRowHeaderWidth write SetRowHeaderWidth default 100;
    //property RowHeight : integer read FRowHeight Write SetRowHeight default 17;
  end;


implementation


//------------------------------------------------------------------------------
{ TGanttItemCollection }
//------------------------------------------------------------------------------


function TGanttItems.Add: TGanttItem;
begin
  Result := TGanttItem(inherited Add);
end;

constructor TGanttItems.Create(GanttControl: TCustomGantt);
begin
  inherited Create(TGanttItem);
  FGanttControl := GanttControl;
end;

function TGanttItems.GetItem(Index: Integer): TGanttItem;
begin
  Result := TGanttItem(inherited GetItem(Index));
end;

function TGanttItems.GetOwner: TPersistent;
begin
  Result := FGanttControl;
end;

procedure TGanttItems.SetItem(Index: Integer; Value: TGanttItem);
begin
  inherited SetItem(Index, Value);
end;

procedure TGanttItems.Update(Item: TCollectionItem);
begin
  if Item <> nil then
    FGanttControl.CalcItemPos(TGanttItem(Item))
  else
    FGanttControl.CalcItemPos;

  FGanttControl.Paint;
  //FGanttControl.Invalidate;
end;


//------------------------------------------------------------------------------
{ TCustomGantt }
//------------------------------------------------------------------------------

{procedure TCustomGantt.SetHeaders(const Value: TGanttHeaders);
begin
  FHeaders := Value;
end; }

procedure TCustomGantt.SetItems(const Value: TGanttItems);
begin
  FItems := Value;
end;

function TCustomGantt.DetectCollision(AItemIndex : integer; ANewRect : TRect) : boolean;
var i : integer;
begin
  result := false;

  for i := 0 to AItemIndex do
    if i <> AItemIndex then
      if Items[i].RowIndex = Items[AItemIndex].RowIndex then
        if IntersectRect(Items[i].ItemRect, ANewRect) then
        //if Items[i].SubRowIndex = Items[AItemIndex].SubRowIndex then
          //if (Items[i].BeginValue < Items[AItemIndex].EndValue) and (Items[i].EndValue > Items[AItemIndex].BeginValue) then
          begin
            result := true;
            Break;
          end;
end;

function TCustomGantt.CalcItemPos(ABeg, AEnd, ARowIndex, AItemIndex : integer): TRect;
var ax, ay, bx, by : integer;
    r : TRect;
    expandCount : integer;
begin
  //base rect
  ax := RowHeaderWidth + (ABeg - FMin) * Scale;
  ay := Rows[ARowIndex].RowRect.Top + 2;
  bx := RowHeaderWidth + (AEnd - FMin) * Scale;
  by := ay + Rows[ARowIndex].Height - 3 - 2;

  r := Rect(ax, ay, bx, by);
  //Rows[ARowIndex].SubRows := 1;

  //rect in expanded row
  if Rows[ARowIndex].Expanded then
  begin
    expandCount := 1;

    while DetectCollision(AItemIndex, r) do
    begin
      ay := ay + Rows[ARowIndex].Height;
      by := by + Rows[ARowIndex].Height;

      r := Rect(ax, ay, bx, by);
      inc(expandCount);
    end;

    if Rows[ARowIndex].SubRows < expandCount then
      Rows[ARowIndex].SubRows := expandCount;
  end;
  //else
    //Rows[ARowIndex].SubRows := 1;

  Result := r;

  inc(FCntI);
end;

procedure TCustomGantt.CalcItemPos(Item: TGanttItem);
begin
  Item.ItemRect := CalcItemPos(Item.BeginValue, Item.EndValue, Item.RowIndex, Item.Index);
end;

procedure TCustomGantt.CalcItemPos;
var i : integer;
begin
  for i := 0 to FItems.Count - 1 do
    CalcItemPos(FItems[i]);
end;

procedure TCustomGantt.CalcRowPos;
var i : integer;
    y, height : integer;
    row : TGanttRow;
begin
  FrozenTop.Clear;

  y := Headers.Height + FrozenTop.Height - VertScroll.Position;

  for i := 0 to Rows.Count - 1 do
  begin
    if y < Headers.Height + FrozenTop.Height then
      if Rows[i].Favorit then
      begin
        FrozenTop.Add(Rows[i]);
      end;

    if Rows[i].Expanded then height := Rows[i].Height * Rows[i].SubRows else height := Rows[i].Height;

    Rows[i].RowHeaderRect := Rect(0, y, RowHeaderWidth, y + height);
    Rows[i].RowRect := Rect(RowHeaderWidth, y, Width - VertScroll.Width - 2, y + height);
    Rows[i].BtFavoritRect := Rect(2, y + 2, 22, y + height - 4);
    Rows[i].BtEnabledRect := Rect(Rows[i].BtFavoritRect.Right, y + 2, Rows[i].BtFavoritRect.Right + 20, y + height - 4);
    Rows[i].BtExpandedRect := Rect(RowHeaderWidth - 20, y + 2, RowHeaderWidth, y + height - 4);

    y := y + height;
  end;

  y := Headers.Height;
  for i := 0 to FrozenTop.Count - 1 do
  begin
    row := FrozenTop[i];
    if row.Expanded then height := row.Height * row.SubRows else height := row.Height;

    FrozenTop[i].RowHeaderRect := Rect(0, y, RowHeaderWidth, y + height);
    FrozenTop[i].RowRect := Rect(RowHeaderWidth, y, Width - VertScroll.Width - 2, y + height);
    FrozenTop[i].BtFavoritRect := Rect(2, y + 2, 22, y + height - 4);
    FrozenTop[i].BtFavoritRect := Rect(2, y + 2, 22, y + height - 4);
    FrozenTop[i].BtEnabledRect := Rect(FrozenTop[i].BtFavoritRect.Right, y + 2, Rows[i].BtFavoritRect.Right + 20, y + height - 4);
    FrozenTop[i].BtExpandedRect := Rect(RowHeaderWidth - 20, y + 2, RowHeaderWidth, y + height - 4);

    y := y + height;
  end;

  inc(FCntR);
end;

constructor TCustomGantt.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMin := 0;
  FMax := 60;
  FScale := 5;
  FZeroDate := Date;
  FHeaders := TGanttHeaders.Create(self);
  FRows := TGanttRows.Create(self);
  FItems := TGanttItems.Create(Self);

  FRowHeaderWidth := 100;
  //FRowHeight := 17;

  FVertScroll := TScrollBar.Create(Self);
  FVertScroll.Parent := Self;
  FVertScroll.Kind := sbVertical;
  FVertScroll.Ctl3D := false;
  FVertScroll.Align := alRight;
  FVertScroll.AlignWithMargins := true;
  FVertScroll.Top := FHeaders.Height + 1;
  FVertScroll.Margins.Right := 1;
  FVertScroll.Margins.Bottom := 0;
  FVertScroll.Margins.Left := 0;
  FVertScroll.Width := 13;
  FVertScroll.OnScroll := Scroll;

  FFrozenTop := TGanttFrozen.Create(Self);

  OnMouseWheelDown := MouseWheelDown;
  OnMouseWheelUp := MouseWheelUp;
end;

destructor TCustomGantt.Destroy;
begin
  FItems.Destroy;
  FRows.Destroy;
  FHeaders.Destroy;
  FVertScroll.Destroy;
  inherited Destroy;
end;

{function TCustomGantt.GetScale: integer;
begin
  Result := Floor((Width - RowHeaderWidth) / (Max - Min));
end; }

procedure TCustomGantt.Invalidate;
var area, rh : integer;
begin
  inherited;

  VertScroll.Margins.Top := Headers.Height;

  if Rows.Height > Height - Headers.Height then
  begin
    VertScroll.PageSize := 0;
    VertScroll.Max := Rows.Height;
    VertScroll.PageSize := Height - Headers.Height;
    if VertScroll.Position > VertScroll.Max - VertScroll.PageSize then
      VertScroll.Position := VertScroll.Max - VertScroll.PageSize;
  end
  else
  begin
    VertScroll.PageSize := 0;
    VertScroll.Max := Height - Headers.Height;
    VertScroll.PageSize := Height - Headers.Height;
    VertScroll.Position := 0;
  end;

  CalcRowPos;
  CalcItemPos;
end;

procedure TCustomGantt.Scroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  if FVertScroll.Max <= FVertScroll.Position + FVertScroll.PageSize then
    FVertScroll.Position := FVertScroll.Max - FVertScroll.PageSize;

  if FVertScroll.Min >= FVertScroll.Position then
    FVertScroll.Position := FVertScroll.Min;

  CalcRowPos;
  CalcItemPos;
  Paint;
end;

procedure TCustomGantt.ScrollHor(Sender: TObject; ScrollCode: TScrollCode;
  var ScrollPos: Integer);
begin
  Paint;
end;

procedure TCustomGantt.MouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
var pos : integer;
begin
  pos := FVertScroll.Position + 50; //1;

  FVertScroll.Position := pos;
  Scroll(sender, scPosition, pos);

  Handled := True;
end;

procedure TCustomGantt.MouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
var pos : integer;
begin
  pos := FVertScroll.Position - 50; //1;

  FVertScroll.Position := pos;
  Scroll(sender, scPosition, pos);

  Handled := True;
end;

procedure TCustomGantt.Resize;
begin
  inherited;
  Invalidate;
end;

procedure TCustomGantt.SetMax(const Value: integer);
begin
  if FMax <> Value then
  begin
    FMax := Value;

    if FMax <= FMin then
      FMin := FMax;

    CalcItemPos;
    Paint;
  end;
end;

procedure TCustomGantt.SetMin(const Value: integer);
begin
  if FMin <> Value then
  begin
    FMin := Value;

    if FMin >= FMax then
      FMax := FMin;

    CalcItemPos;
    Paint;
  end;
end;

{procedure TCustomGantt.SetRowHeight(const Value: integer);
begin
  FRowHeight := Value;
  CalcItemPos;
  Paint;
end; }


procedure TCustomGantt.SetRowHeaderWidth(const Value: integer);
begin
  if Value < 40 then
    FRowHeaderWidth := 40
  else
    FRowHeaderWidth := Value;

  CalcRowPos;
  CalcItemPos;
  Paint;
end;

procedure TCustomGantt.SetScale(const Value: integer);
begin
  if Value <= 0 then
    FScale := 1
  else
    FScale := Value;

  //Invalidate;
  CalcRowPos;
  CalcItemPos;
  Paint;
end;

function TCustomGantt.GetAltColor(AColor : TColor; AValue : byte = 15): TColor;
var Y, Cb, Cr : byte;
    R, G, B : byte;
    v : Integer;
begin
  //check for system color
  AColor := ColorToRGB(AColor);

  // RGB -> YCbCr
  R := AColor;
  G := AColor shr 8;
  B := AColor shr 16;

  Y := Trunc( 0.299  * R + 0.587  * G + 0.114 * B); // ����� �������
  Cb := Trunc(-0.1687 * R - 0.3313 * G + 0.5   * B + 128.0);
  Cr := Trunc( 0.5    * R - 0.4187 * G - 0.0813* B + 128.0);

  if Y > 120 then Y := Y - AValue else Y := Y + AValue;

  // YCbCr -> RGB
  v := Trunc(Y + 1.772 * (Cb - 128.0));
  if v > 255 then
    v := 255
  else if v < 0
    then v := 0;
  B := v;

  v := Trunc(Y - 0.34414 * (Cb - 128.0) - 0.71414 * (Cr - 128.0));
  if v > 255 then
    v := 255
  else if v < 0
    then v := 0;
  G := v;

  v := Trunc(Y + 1.402 * (Cr - 128.0));
  if v > 255 then
    v := 255
  else if v < 0 then
    v := 0;
  R := v;

  result := (r or (g shl 8) or (b shl 16));
end;

end.
