﻿unit Gantt.Row;

interface

uses
  System.Classes, System.Types, Vcl.Graphics, Gantt.ItemCollection,
  Gantt.Graphics;

type
  TRowHeaderButton = (rbtNone, rbtFavorit, rbtEnabled, rbtExpanded);

type
  TGanttRow = class(TCollectionItem)
  private
    FHeight : integer;
    FSubRows : integer;
    FText : string;
    FTextAlign : TAlignment;
    FFontColor : TColor;
    FColor : TColor;
    FBorderColor : TColor;
    FRowRect : TRect;
    FRowHeaderRect : TRect;

    FFavorit : boolean;
    FBtFavoritRect : TRect;
    FEnabled : boolean;
    FBtEnabledRect : TRect;
    FExpanded : boolean;
    FBtExpandedRect : TRect;

    procedure SetHeight(const Value : integer);
    procedure SetText(const Value : string);
    procedure SetTextAlign(const Value : TAlignment);
    procedure SetFontColor(const Value : TColor);
    procedure SetColor(const Value : TColor);
    procedure SetBorderColor(const Value : TColor);
    procedure SetRowRect(const Value : TRect);
    procedure SetFavorit(const Value : boolean);
    procedure SetEnabled(const Value : boolean);
    procedure SetExpanded(const Value : boolean);
    procedure SetSubRows(const Value : integer);
    function GetTotalHeight : integer;
  public
    constructor Create(Collection: TCollection); override;
    property RowRect : TRect read FRowRect write SetRowRect;
    property RowHeaderRect : TRect read FRowHeaderRect write FRowHeaderRect;
    property BtFavoritRect : TRect read FBtFavoritRect write FBtFavoritRect;
    property BtEnabledRect : TRect read FBtEnabledRect write FBtEnabledRect;
    property BtExpandedRect : TRect read FBtExpandedRect write FBtExpandedRect;

    //property TotalHeight : integer read GetTotalHeight;
    procedure Paint(ACanvas : TCanvas; isActive : boolean; AButton : TRowHeaderButton; AGridLineColor : TColor);
  published
    property Height : integer read FHeight write SetHeight default 30;
    property Text : string read FText write SetText;
    property TextAlignment : TAlignment read FTextAlign write SetTextAlign default taLeftJustify;
    property FontColor : TColor read FFontColor write SetFontColor default clBlack;
    property Color : TColor read FColor write SetColor default clWhite;
    property BorderColor : TColor read FBorderColor write SetBorderColor default cl3DLight;
    property SubRows : integer read FSubRows write SetSubRows default 1;

    property Favorit : boolean read FFavorit write SetFavorit default false;
    property Enabled : boolean read FEnabled write SetEnabled default true;
    property Expanded : boolean read FExpanded write SetExpanded default false;
  end;

implementation

{ TGanttRow }

constructor TGanttRow.Create(Collection: TCollection);
begin
  FHeight := 30;
  FText := '';
  FTextAlign := taLeftJustify;
  FFontColor := clBlack;
  FColor := clWhite;
  FBorderColor := cl3DLight;
  FSubRows := 1;

  FFavorit := false;
  FEnabled := true;
  FExpanded := false;

  inherited Create(Collection);
end;

function TGanttRow.GetTotalHeight: integer;
begin
  result := Height * SubRows;
end;

procedure TGanttRow.SetExpanded(const Value: boolean);
begin
  if FExpanded <> Value then
  begin
    FExpanded := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetBorderColor(const Value: TColor);
begin
  if FBorderColor <> Value then
  begin
    FBorderColor := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetColor(const Value: TColor);
begin
  if FColor <> Value then
  begin
    FColor := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetEnabled(const Value: boolean);
begin
  if FEnabled <> Value then
  begin
    FEnabled := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetFavorit(const Value: boolean);
begin
  if FFavorit <> Value then
  begin
    FFavorit := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetFontColor(const Value: TColor);
begin
  if FFontColor <> Value then
  begin
    FFontColor := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetHeight(const Value: integer);
begin
  if FHeight <> Value then
  begin
    FHeight := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetRowRect(const Value: TRect);
begin
  if FRowRect <> Value then
  begin
    FRowRect := Value;
    //Changed(false);
  end;
end;

procedure TGanttRow.SetSubRows(const Value: integer);
var Val : integer;
begin
  if Value <= 0 then
    Val := 1
  else
    Val := Value;

  if FSubRows <> Val then
  begin
    FSubRows := Val;
    Changed(false);
  end;
end;

procedure TGanttRow.SetText(const Value: string);
begin
  if FText <> Value then
  begin
    FText := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.SetTextAlign(const Value: TAlignment);
begin
  if FTextAlign <> Value then
  begin
    FTextAlign := Value;
    Changed(false);
  end;
end;

procedure TGanttRow.Paint(ACanvas : TCanvas; isActive : boolean; AButton : TRowHeaderButton; AGridLineColor : TColor);
var al : TTextFormats;
    favorRect, enabledRect, expRect, textRect : TRect;
    var text : string;
    fs : integer;
begin
  if isActive then
    ACanvas.Brush.Color := GetAltColor(Color, 10)
  else
    ACanvas.Brush.Color := Color;

  ACanvas.Pen.Color := BorderColor;
  ACanvas.Pen.Style := psSolid;
  ACanvas.Font.Color := FontColor;

  //fill row header
  ACanvas.FillRect(RowHeaderRect);


  //paint Favorit
  favorRect := BtFavoritRect;// Rect(ARow.RowHeaderRect.Left, ARow.RowHeaderRect.Top, ARow.RowHeaderRect.Left + 18, ARow.RowHeaderRect.Bottom);
  text := '★';
  fs := ACanvas.Font.Size;
  ACanvas.Font.Size := ACanvas.Font.Size + 6;

  if Favorit then
    ACanvas.Font.Color := clHighlight
  else
    ACanvas.Font.Color := GetAltColor(ACanvas.Brush.Color, 20);

  if isActive and (AButton = rbtFavorit) then
    ACanvas.Font.Color := GetAltColor(ACanvas.Font.Color, 50);

  ACanvas.TextRect(favorRect, text, [tfCenter, tfVerticalCenter, tfSingleLine]);
  ACanvas.Font.Size := fs;
  ACanvas.Font.Color := FontColor;


  //paint Enabled
  enabledRect := BtEnabledRect; //Rect(favorRect.Right, ARow.RowHeaderRect.Top, favorRect.Right + 18, ARow.RowHeaderRect.Bottom);
  text := '🔒';

  if not Enabled then
    ACanvas.Font.Color := clGray
  else
    ACanvas.Font.Color := GetAltColor(ACanvas.Brush.Color, 20);

  if (isActive) and (AButton = rbtEnabled) then
    ACanvas.Font.Color := GetAltColor(ACanvas.Font.Color, 50);

  ACanvas.TextRect(enabledRect, text, [tfCenter, tfVerticalCenter, tfSingleLine]);
  ACanvas.Font.Color := FontColor;


  //paint Expanded
  expRect := BtExpandedRect; //Rect(favorRect.Right, ARow.RowHeaderRect.Top, favorRect.Right + 18, ARow.RowHeaderRect.Bottom);
  ACanvas.Font.Size := ACanvas.Font.Size + 4;

  if SubRows <= 1 then
    text := ''
  else if Expanded then
  begin
    //ACanvas.Font.Color := clMedGray;
    text := '🞃';
  end
  else
  begin
    //ACanvas.Font.Color := GetAltColor(ACanvas.Brush.Color, 25);
    text := '🞂';
  end;
  ACanvas.Font.Color := clMedGray;

  if (isActive) and (AButton = rbtExpanded) then
    ACanvas.Font.Color := GetAltColor(ACanvas.Font.Color, 50);

  ACanvas.TextRect(expRect, text, [tfCenter, tfVerticalCenter, tfSingleLine]);
  ACanvas.Font.Color := FontColor;
  ACanvas.Font.Size := fs;

  //paint text
  textRect := Rect(enabledRect.Right + 3, RowHeaderRect.Top, expRect.Left - 1, RowHeaderRect.Bottom - 1);

  //if Assigned(FOnRowHeaderPaint) then
  //  FOnRowHeaderPaint(ARow.Index, textRect, ACanvas)
  //else
  begin
    text := FText;
    if TextAlignment = taLeftJustify then al := tfLeft
    else if TextAlignment = taRightJustify then al := tfRight
    else al := tfCenter;

    ACanvas.TextRect(textRect, text, [al, tfVerticalCenter, tfSingleLine, tfEndEllipsis]);
  end;

  //fill row
  ACanvas.FillRect(RowRect);

  //underline
  //if (FrozenTop.Height > 0) and (ARow.RowRect.Bottom = Headers.Height + FrozenTop.Height) then
  //  ACanvas.Pen.Color := GridLineColor;

  ACanvas.MoveTo(0, RowRect.Bottom - 1);
  ACanvas.LineTo(RowRect.Right, RowRect.Bottom - 1);

  //left border
  ACanvas.Pen.Color := AGridLineColor;
  ACanvas.MoveTo(0, RowRect.Top);
  ACanvas.LineTo(0, RowRect.Bottom);

  //delimeter line
  ACanvas.MoveTo(RowHeaderRect.Right, RowRect.Top);
  ACanvas.LineTo(RowHeaderRect.Right, RowRect.Bottom);
end;

end.
