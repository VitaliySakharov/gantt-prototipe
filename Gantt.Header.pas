unit Gantt.Header;

interface

uses
  System.Classes, Vcl.Controls, Vcl.Graphics, SysUtils, DateUtils, Math;

type
  TGanttHeader = class(TCollectionItem)
  private
    FHeight : integer;
    FStep : integer;
    FMask : string;
    FFontColor : TColor;
    FColor : TColor;
    FBorderColor : TColor;
    procedure SetHeight(const Value : integer);
    procedure SetStep(const Value : integer);
    procedure SetMask(const Value : string);
    procedure SetFontColor(const Value : TColor);
    procedure SetColor(const Value : TColor);
    procedure SetBorderColor(const Value : TColor);
  public
    constructor Create(Collection: TCollection); override;

  published
    property Height : integer read FHeight write SetHeight default 20;
    property Step : integer read FStep write SetStep default 5;
    property Mask : string read FMask write SetMask;
    property FontColor : TColor read FFontColor write SetFontColor default clBlack;
    property Color : TColor read FColor write SetColor default clWhite;
    property BorderColor : TColor read FBorderColor write SetBorderColor default cl3DLight;

    function IntToDateTimeFormat(FZeroDate : TDate; Value : integer) : string;
  end;

implementation

{ TGanttHeader }

constructor TGanttHeader.Create(Collection: TCollection);
begin
  FHeight := 20;
  FStep := 15;
  FMask := 'h:nn';
  FFontColor := clBlack;
  FColor := clWhite;
  FBorderColor := cl3DLight;

  inherited Create(Collection);
end;

procedure TGanttHeader.SetBorderColor(const Value: TColor);
begin
  if FBorderColor <> Value then
  begin
    FBorderColor := Value;
    Changed(false);
  end;
end;

procedure TGanttHeader.SetColor(const Value: TColor);
begin
  if FColor <> Value then
  begin
    FColor := Value;
    Changed(false);
  end;
end;

procedure TGanttHeader.SetFontColor(const Value: TColor);
begin
  if FFontColor <> Value then
  begin
    FFontColor := Value;
    Changed(false);
  end;
end;

procedure TGanttHeader.SetHeight(const Value: integer);
begin
  if FHeight <> Value then
  begin
    FHeight := Value;
    Changed(false);
  end;
end;

procedure TGanttHeader.SetMask(const Value: string);
begin
  if FMask <> Value then
  begin
    FMask := Value;
    Changed(false);
  end;
end;

procedure TGanttHeader.SetStep(const Value: integer);
begin
  if FStep <> Value then
  begin
    FStep := Value;
    Changed(false);
  end;
end;

function TGanttHeader.IntToDateTimeFormat(FZeroDate : TDate; Value : integer): string;
var cur : TDateTime;
begin
  if FZeroDate <> 0 then
  begin
    cur := EncodeDateTime(YearOf(FZeroDate), MonthOf(FZeroDate), DayOf(FZeroDate), 0, 0, 0, 0);
    cur := IncMinute(cur, Value);
    result := FormatDateTime(FMask, cur);
  end
  else
    result := IntToStr(Value);
end;

end.
